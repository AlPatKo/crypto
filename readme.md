# CryptoChat (Or So They Say)

Disclaimer: Unfortunately, at this point crypto part of the chat doesn't work, This happens due to an error I can't seem to comprehend.


## Algorithm

For the purpose of encryption I decided to use both symmetric and asymmetric encryption. 

Steps:

1) Client issues a request to go crypto

2) Client sends his public key to the server

3) Server generates an AES cipher and encrypts both key and initialization vector using clients public key

4) Server sends encrypted secrets over the network to the client

5) Client decrypts those messages using his private key and obtains secrets

6) Further communication is encrypted using AES cipher and is decrypted by means of the decipher with the same key and IV

## Issue Encountered

While executing this lab, I decided to send only public key and not a certificate, since intuitively, if we send only public key, intercepting party can't get anything out of it, whereas if they intercept an entire certificate, they get our private key, which is super dooper bad.

So, when at server side I try to recreate the RSA public key from pem that I sent I get identic public keys ob both sides(Verified using hashes), however I get an issue stating that padding check is failed, which I found out, after some googling, to be happening when the we attempt to decrypt with a private key which is not associated to the public key used in encryption. So this is quite strange and I can't seem to get hold of solution to this problem.