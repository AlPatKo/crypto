require 'socket'
require 'openssl'
require 'base64'
require 'digest'

class Server

  attr_accessor :client, :client_pub, :crypto, :iv, :secret

  def initialize
    server = TCPServer.new 'localhost', 5000

    @client = server.accept
    @encrypted = false
  end

  def run
    loop do 
      phrase = get_message.chomp
      if phrase == 'kbye'
        client.close
        return
      elsif phrase == 'pwn'
        puts phrase
        go_cryptic        
      else
        puts phrase
        send_message "Man, I am sorry but my vocabulary is fairly limited, hence it is going to get repetitive, eventually =("
      end
    end
  end

  def get_message
    msg = client.gets
    if @encrypted
      message = crypto[:decipher].update Base64.strict_decode64 msg
      message << crypto[:decipher].final
      msg = message
    end
    msg
  end

  def send_message(msg)
    if @encrypted
      message = crypto[:cipher].update msg
      message << crypto[:cipher].final
      client.puts Base64.strict_encode64 message
    else
      client.puts msg
    end
  end

  def get_pub
    pem = ''
    loop do
      pem_line = get_message
      pem += pem_line
      break if pem_line.include?("-----END PUBLIC KEY-----")
    end
    @client_pub = OpenSSL::PKey::RSA.new(pem.chomp)
    puts '||||||||||||||||||'
    puts Digest::SHA256.hexdigest client_pub.to_s
  end

  def send_secrets
    @crypto = {:cipher => OpenSSL::Cipher::AES.new(128, :CBC).encrypt,
              :decipher => OpenSSL::Cipher::AES.new(128, :CBC).decrypt}
    @iv = @crypto[:decipher].iv = @crypto[:cipher].random_iv
    @secret = @crypto[:decipher].key = @crypto[:cipher].random_key
    send_message Base64.encode64(@client_pub.public_encrypt(iv))
    send_message Base64.encode64(@client_pub.public_encrypt(secret))
  end

  def go_cryptic
    send_message "Oh Man, we're going off the grid, I am so excited!"
    get_pub
    send_secrets
    @encrypted = true
  end
end

Server.new.run