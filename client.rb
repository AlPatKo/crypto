require 'socket'
require 'openssl'
require 'base64'
require 'digest'

class Client

  attr_accessor :socket, :priv_key, :pub_key, :iv, :secret, :crypto

  def initialize
    @socket = TCPSocket.new 'localhost', 5000
    @crypto = {:cipher => OpenSSL::Cipher::AES.new(128, :CBC).encrypt,
              :decipher => OpenSSL::Cipher::AES.new(128, :CBC).decrypt}
    @encrypted = false
  end

  def run
    loop do 
      msg = gets.chomp
      next if msg.empty?
      send_message msg

      if msg == 'kbye'
        socket.close
        return
      elsif msg == 'pwn'
        go_cryptic
      else
        puts get_message
      end    
    end
  end

  def send_message(msg)
    if @encrypted
      message = crypto[:cipher].update msg
      message << crypto[:cipher].final
      socket.puts Base64.strict_encode64 message
    else
      socket.puts msg
    end
  end

  def get_message
    msg = socket.gets
    if @encrypted
      message = crypto[:decipher].update Base64.strict_decode64 msg.chomp
      message << crypto[:decipher].final
      msg = message
    end
    msg
  end

  def send_pub
    @priv_key = OpenSSL::PKey::RSA.new 1024
    @pub_key = priv_key.public_key
    puts '||||||||||||||'
    puts Digest::SHA256.hexdigest pub_key.to_s
    send_message pub_key.to_pem
  end

  def get_secrets
    iv_enc = get_message
    secret_enc = get_message
    @iv = priv_key.private_decrypt(Base64.decode64(iv_enc))
    @secret = priv_key.private_decrypt(Base64.decode64(secret_enc))
    @crypto[:cipher].key = @crypto[:decipher].key = secret
    @crypto[:cipher].iv = @crypto[:decipher].iv = iv
  end

  def go_cryptic
    puts get_message
    send_pub
    get_secrets
    @encrypted = true
  end

end

Client.new.run